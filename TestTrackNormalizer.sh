#!/bin/bash

time osascript "Normalize iTunes track names.scpt" --dry-run
diff "test/expected normalized track names.txt" "test/actual normalized track names.txt"

