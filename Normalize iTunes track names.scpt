use scripting additions
use framework "Foundation"
--use framework "AppKit"

global CRLF, PS, DEBUG, ATTR_SEPARATOR, ATTR_COLON, ATTR_ORIGINAL_NAME, ATTR_SHELF, DRY_RUN

on run argv
	set CRLF to "
"
	set PS to ":"
	set DEBUG to true
	set ATTR_SEPARATOR to CRLF
	set ATTR_COLON to ": "
	set ATTR_ORIGINAL_NAME to "OriginalName"
	set ATTR_SHELF to "Shelf"
	set DRY_RUN to argv contains "--dry-run"
	--try
	my normalizeClassicalTrackNames()
	--on error _error
	--	log "Error: " & _error
	--end try
end run

--
--
--
to normalizeClassicalTrackItalianName(_name)
	set _name to my replaceText("#", "n° ", _name)
	set _name to my replaceWords("No. ", "n° ", _name)
	set _name to my replaceWords("No.", "n° ", _name)
	--	set _name to my replaceWords("No ", "n° ", _name)

	set _name to my replaceWords("In D major", "in re", _name)
	set _name to my replaceWords("In D minor", "in re minore", _name)
	set _name to my replaceWords("In D", "in re", _name)

	set _name to my replaceWords("In E flat major", "in mi♭", _name)
	set _name to my replaceWords("In E flat minor", "in mi♭ minore", _name)
	set _name to my replaceWords("In E flat", "in mi♭", _name)
	set _name to my replaceWords("In E major", "in mi", _name)
	set _name to my replaceWords("In E minor", "in mi minore", _name)
	set _name to my replaceWords("In E", "in mi", _name)

	set _name to my replaceWords("In F major", "in fa", _name)
	set _name to my replaceWords("In F minor", "in fa minore", _name)
	set _name to my replaceWords("In F", "in fa", _name)

	set _name to my replaceWords("In G major", "in sol", _name)
	set _name to my replaceWords("In G minor", "in sol minore", _name)
	set _name to my replaceWords("In G", "in sol", _name)

	set _name to my replaceWords("In A major", "in la", _name)
	set _name to my replaceWords("In A minor", "in la minore", _name)
	set _name to my replaceWords("In A Flat", "in la♭", _name)
	set _name to my replaceWords("In A", "in la", _name)

	set _name to my replaceWords("In B flat major", "in si♭", _name)
	set _name to my replaceWords("In B flat minor", "in si♭ minore", _name)
	set _name to my replaceWords("In B flat", "in si♭", _name)
	set _name to my replaceWords("In Bb major", "in si♭", _name)
	set _name to my replaceWords("In Bb minor", "in si♭ minore", _name)
	set _name to my replaceWords("In Bb", "in si♭", _name)
	set _name to my replaceWords("In B major", "in si", _name)
	set _name to my replaceWords("In B minor", "in si minore", _name)
	set _name to my replaceWords("In B", "in si", _name)

	set _name to my replaceWords("In C major", "in do", _name) -- at last, otherwise 'Do' will be substituted
	set _name to my replaceWords("In C minor", "in do minore", _name)
	set _name to my replaceWords("In C", "in do", _name)

	set _name to my replaceWords("In Re Minore", "in re minore", _name)
	set _name to my replaceWords("In Re Maggiore", "in re", _name)
	set _name to my replaceWords("In La Minore", "in la minore", _name)
	set _name to my replaceWords("In La Maggiore", "in la", _name)
	set _name to my replaceWords("In Fa Minore", "in fa minore", _name)
	set _name to my replaceWords("In Fa Maggiore", "in fa", _name)

	set _name to my replaceWords("À 2 Violoncelli", "per due violoncelli", _name)
	set _name to my replaceWords("Fugue For 2 Violoncellos", "Fuga per due violoncelli", _name)
	set _name to my replaceWords("For Two Violins", "per due violini", _name)
	set _name to my replaceWords("For 2 Violins", "per due violini", _name)
	set _name to my replaceWords("For 2 Violinos", "per due violini", _name)
	set _name to my replaceWords("For Three Violins", "per tre violini", _name)
	set _name to my replaceWords("For 3 Violins", "per tre violini", _name)
	set _name to my replaceWords("For Four Violins", "per quattro violini", _name)
	set _name to my replaceWords("For 4 Violins", "per quattro violini", _name)

	set _name to my replaceWords("For Two Cellos", "per due violoncelli", _name)
	set _name to my replaceWords("For 2 Cellos", "per due violoncelli", _name)
	set _name to my replaceWords("2 Cellos", "due violoncelli", _name)
	set _name to my replaceWords("For Three Cellos", "per tre violoncelli", _name)
	set _name to my replaceWords("For 3 Cellos", "per tre violoncelli", _name)
	set _name to my replaceWords("for 2 Oboes", "per due oboe", _name)
	set _name to my replaceWords("For 2 Mandolins", "per due mandolini", _name)
	set _name to my replaceWords("For Viola D'Amore", "per viola d'amore", _name)

	set _name to my replaceWords("Canzon", "Canzone", _name)
	set _name to my replaceWords("Canon", "Canone", _name)
	set _name to my replaceWords("Cello Concerto", "Concerto per violoncello", _name)
	set _name to my replaceWords("Sonata For Violoncello And Basso Continuo", "Sonata per violoncello e basso continuo", _name)
	set _name to my replaceWords("Cello Sonata", "Sonata per violoncello", _name)
	set _name to my replaceWords("Flute Concerto", "Concerto per flauto", _name)
	set _name to my replaceWords("Mandolin Concerto", "Concerto per mandolino", _name)
	set _name to my replaceWords("Oboe Concerto", "Concerto per oboe", _name)
	set _name to my replaceWords("Violin Concerto", "Concerto per violino", _name)

	set _name to my replaceWords("Concerto a 5", "Concerto a cinque", _name)
	set _name to my replaceWords("Concerto Grosso", "Concerto grosso", _name)

	set _name to my replaceWords("Cellos", "violoncelli", _name)
	set _name to my replaceWords("Cello", "violoncello", _name)
	set _name to my replaceWords("Flute", "flauto", _name)
	set _name to my replaceWords("Lute", "liuto", _name)
	set _name to my replaceWords("Oboe", "oboe", _name)
	set _name to my replaceWords("Salmoe", "salmoè", _name)
	set _name to my replaceWords("Strings", "archi", _name)
	set _name to my replaceWords("Winds", "fiati", _name)
	set _name to my replaceWords("Three Viole All'inglese", "tre viole all'inglese", _name)
	set _name to my replaceWords("Violin", "violino", _name)
	set _name to my replaceWords("Orchestra", "orchestra", _name)
	set _name to my replaceWords("and Continuo", "e basso continuo", _name)

	set _name to my replaceWords("24 Caprices, Op. 1 - Capriccio", "Capriccio Op. 1", _name)


	set _name to my replaceWords("For", "per", _name)

	set _name to my normalizeTempo(_name)

	set _name to my replaceText(" & ", " e ", _name)
	set _name to my replaceText(", e ", " e ", _name)

	return _name
end normalizeClassicalTrackItalianName

--
--
--
to normalizeClassicalTrackGermanName(_name, _track, _composer)
	set _name to my replaceWords("Major", "major", _name)
	set _name to my replaceWords("Minor", "minor", _name)
	set _name to my replaceWords("Flat", "flat", _name)
	set _name to my replaceWords("Sharp", "sharp", _name)
	set _name to my replaceWords("C-Sharp", "C sharp", _name)
	set _name to my replaceWords("D-Sharp", "D sharp", _name)
	set _name to my replaceWords("D-Major", "D major", _name)
	set _name to my replaceWords("A-Major", "A major", _name)
	set _name to my replaceWords("F-Sharp", "F sharp", _name)
	set _name to my replaceWords("G-Sharp", "G sharp", _name)
	set _name to my replaceWords("A-Flat", "A flat", _name)
	set _name to my replaceWords("B-Flat", "B flat", _name)
	set _name to my replaceWords("E-Flat", "E flat", _name)

	-- http://www.urfm.braidense.it/documentazione/lezioni/Titoloconvenzionale/tonalita.htm
	set _name to my replaceWords("In C major", "in C-Dur", _name)
	set _name to my replaceWords("In C minor", "in c-Moll", _name)
	set _name to my replaceWords("In C flat major", "in Ces-Dur", _name)
	set _name to my replaceWords("In C flat minor", "in ces-Moll", _name)
	set _name to my replaceWords("In C flat", "in Ces-Dur", _name)
	set _name to my replaceWords("In C sharp major", "in Cis-Dur", _name)
	set _name to my replaceWords("In C sharp minor", "in cis-Moll", _name)
	set _name to my replaceWords("In C sharp", "in Cis-Dur", _name)
	set _name to my replaceText(" In C ", " in C-Dur ", _name)
	set _name to my replaceText(" In C,", " in C-Dur,", _name)

	set _name to my replaceWords("In D major", "in D-Dur", _name)
	set _name to my replaceWords("In D minor", "in d-Moll", _name)
	set _name to my replaceWords("In D flat major", "in Des-Dur", _name)
	set _name to my replaceWords("In D flat minor", "in des-Moll", _name)
	set _name to my replaceWords("In D flat", "in Des-Dur", _name)
	set _name to my replaceWords("In D sharp major", "in Dis-Dur", _name)
	set _name to my replaceWords("In D sharp minor", "in dis-Moll", _name)
	set _name to my replaceWords("In D sharp", "in Dis-Dur", _name)
	set _name to my replaceText(" In D ", " in D-Dur ", _name)
	set _name to my replaceText(" In D,", " in D-Dur,", _name)

	set _name to my replaceWords("In E major", "in E-Dur", _name)
	set _name to my replaceWords("In E minor", "in e-Moll", _name)
	set _name to my replaceWords("In E flat major", "in Es-Dur", _name)
	set _name to my replaceWords("In E flat minor", "in es-Moll", _name)
	set _name to my replaceWords("In E flat", "in Es-Dur", _name)
	set _name to my replaceText(" In E ", " in E-Dur ", _name)
	set _name to my replaceText(" In E,", " in E-Dur,", _name)

	set _name to my replaceWords("In F major", "in F-Dur", _name)
	set _name to my replaceWords("In F minor", "in f-Moll", _name)
	set _name to my replaceWords("In F sharp major", "in Fis-Dur", _name)
	set _name to my replaceWords("In F sharp minor", "in fis-Moll", _name)
	set _name to my replaceWords("In F sharp", "in Fis-Dur", _name)
	set _name to my replaceText(" In F ", " in F-Dur ", _name)
	set _name to my replaceText(" In F,", " in F-Dur,", _name)

	set _name to my replaceWords("In G major", "in G-Dur", _name)
	set _name to my replaceWords("In G minor", "in g-Moll", _name)
	set _name to my replaceWords("In G flat major", "in Ges-Dur", _name)
	set _name to my replaceWords("In G flat minor", "in ges-Moll", _name)
	set _name to my replaceWords("In G flat", "in Ges-Dur", _name)
	set _name to my replaceWords("In G sharp major", "in Gis-Dur", _name)
	set _name to my replaceWords("In G sharp minor", "in gis-Moll", _name)
	set _name to my replaceWords("In G sharp", "in Gis-Dur", _name)
	set _name to my replaceText(" In G ", " in G-Dur ", _name)
	set _name to my replaceText(" In G,", " in G-Dur,", _name)

	set _name to my replaceWords("In A major", "in A-Dur", _name)
	set _name to my replaceWords("In A minor", "in a-Moll", _name)
	set _name to my replaceWords("In A flat major", "in As-Dur", _name)
	set _name to my replaceWords("In A flat minor", "in as-Moll", _name)
	set _name to my replaceWords("In A flat", "in As-Dur", _name)
	set _name to my replaceWords("In A sharp major", "in Ais-Dur", _name)
	set _name to my replaceWords("In A sharp minor", "in ais-Moll", _name)
	set _name to my replaceWords("In A sharp", "in Ais-Dur", _name)
	set _name to my replaceText(" In A ", " in A-Dur ", _name)
	set _name to my replaceText(" In A,", " in A-Dur,", _name)

	set _name to my replaceWords("In B major", "in H-Dur", _name)
	set _name to my replaceWords("In B minor", "in h-Moll", _name)
	set _name to my replaceWords("In B flat major", "in B-Dur", _name)
	set _name to my replaceWords("In B flat minor", "in b-Moll", _name)
	set _name to my replaceWords("In B flat", "in B-Dur", _name)
	set _name to my replaceText(" In B ", " in H-Dur ", _name)
	set _name to my replaceText(" In B,", " in H-Dur,", _name)

	set _name to my replaceWords("Piano Concerto", "Klavierkonzert", _name)
	set _name to my replaceWords("Violin Concerto", "Violinkonzert", _name)
	set _name to my replaceWords("Symphony", "Sinfonie", _name)
	set _name to my replaceWords("String Quartet", "Streichquartett", _name)
	set _name to my replaceWords("Arpeggione Sonata", "Sonate für Arpeggione und Klavier", _name)
	set _name to my replaceWords("Piano Sonata", "Klaviersonate", _name)

	set _name to my replaceWords("Fünf Stücke Im Volkston", "Fünf Stücke im Volkston (Cinque pezzi in stile popolare)", _name)
	set _name to my replaceWords("Rhenish", "Rheinische Sinfonie", _name)
	set _name to my replaceWords("Piano Trio", "Klaviertrio", _name)
	set _name to my replaceWords("Spring", "Frühlingssinfonie (Primavera)", _name)
	set _name to my replaceWords("Death & The Maiden''", "Der Tod und das Mädchen\"", _name)
	set _name to my replaceWords("Unfinished", "Die Unvollendete", _name)
	set _name to my replaceWords("Blumenstück", "Blumenstück (Ai fiori)", _name)

	set _name to my replaceWords("Concerto for Violin, Cello and Piano", "Konzert für Klavier, Violine, Violoncello und Orchester", _name)
	set _name to my replaceWords("Triple Concerto", "Tripelkonzert", _name)
	set _name to my replaceWords("\"Choral\"", "\"An die Freude\"", _name)
	set _name to my replaceWords("Tempest", "Der Sturm", _name)
	set _name to my replaceWords("Moonlight", "Mondscheinsonate (Al chiaro di luna)", _name)
	set _name to my replaceWords("Pastoral", "Pastorale", _name)
	set _name to my replaceWords("Violin Romance", "Violinromanze", _name)
	set _name to my replaceWords("Romance for violin & orchestra", "Violinromanze", _name)

	set _name to my replaceWords("Sonata No. 27", "Klaviersonate No. 27", _name)
	set _name to my replaceWords("Sonata No. 28", "Klaviersonate No. 28", _name)
	set _name to my replaceWords("Six Variation", "Sechs Variationen über ein eigenes Thema", _name)
	set _name to my replaceWords("Fifteen Variations And Fugue", "Fünfzehn Variationen mit Finale alla Fuga", _name)
	set _name to my replaceWords("\"Eroica\" Variations", "\"Eroica-Variationen\"", _name)

	set _name to my replaceWords("For 2 Violins", "für zwei Violinen", _name)
	set _name to my replaceWords("For 2 Harpsichords", "für zwei Cembali", _name)
	set _name to my replaceWords("For 3 Harpsichords", "für drei Cembali", _name)
	set _name to my replaceWords("For 4 Harpsichords", "für vier Cembali", _name)
	set _name to my replaceWords("The Well-Tempered Clavier", "Well-Tempered Clavier", _name)
	set _name to my replaceWords("Well-Tempered Clavier, Book 2", "Das Wohltemperierte Klavier, Buch 2", _name)
	set _name to my replaceWords("Well-Tempered Clavier, Book II", "Das Wohltemperierte Klavier, Buch 2", _name)
	set _name to my replaceWords("Well-Tempered Clavier, Book 1", "Das Wohltemperierte Klavier, Buch 1", _name)
	set _name to my replaceWords("Well-Tempered Clavier, Book I", "Das Wohltemperierte Klavier, Buch 1", _name)
	set _name to my replaceWords("Prelude and Fugue", "Präludium und Fuge", _name)
	set _name to my replaceWords("Prelude & Fugue", "Präludium und Fuge", _name)
	set _name to my replaceWords("Brandenburg Concerto", "Brandenburgische Konzert", _name)
	set _name to my replaceWords("Orchestral Suite", "Orchestersuite", _name)
	set _name to my replaceWords("Goldberg Variations", "Goldberg-Variationen", _name)
	set _name to my replaceWords("Toccata & Fugue", "Toccata und Fuge", _name)
	set _name to my replaceWords("Toccata, Adagio & Fugue", "Toccata, Adagio und Fuge", _name)
	set _name to my replaceWords("Lute Suite", "Lauten-Suite", _name)
	set _name to my replaceWords("Lute Partita", "Lauten-Suite", _name) -- CHECK!
	set _name to my replaceWords("2-Part Invention", "Zweistimmige Invention", _name)
	set _name to my replaceWords("3-Part Invention", "Dreistimmige Invention", _name)
	set _name to my replaceWords("The Art Of Fugue", "Die Kunst der Fuge", _name)
	set _name to my replaceWords("Cello Suite", "Suite für Violoncello solo", _name)
	set _name to my replaceWords("Mass", "Messe", _name)
	set _name to my replaceWords("Prelude, Fugue & Allegro", "Präludium, Fuge und Allegro", _name)

	set _name to my replaceWords("Concerto", "Konzert", _name)
	set _name to my replaceWords("Cantata", "Kantate", _name)
	set _name to my replaceWords("Christmas Oratorio", "Weihnachtsoratorium", _name)

	set _name to my replaceWords("Trio Sonata", "Triosonate", _name)

	set _name to my replaceWords("Präludium Und Fuge In G-Moll", "Präludium Und Fuge g-Moll", _name)

	set _name to my replaceText("In modo di scena cantante", "In Form einer Gesangsszene", _name)

	if (_composer is "Beethoven, Ludwig van") then
		set _name to my replaceText("33 Variations in C-Dur on a Waltz by Anton Diabelli", "Diabelli-Variationen", _name)
		set _name to my replaceWords("Sonata", "Klaviersonate", _name)
		set _name to my replaceWords("Violin Klaviersonate", "Violinsonate", _name)
		set _name to my replaceWords("Grand Klaviersonate", "Grand Sonata", _name)
	end if

	-- Bach
	set _bwv to my getBWVFrom(_name)

	set _name to my replaceWords("Sonata For Solo Violin", "Sonate für Violine solo", _name)
	set _name to my replaceWords("Partita For Solo Violin", "Partita für Violine solo", _name)
	set _name to my replaceWords("Violin Partita", "Partita für Violine solo", _name)

	set _name to my replaceWords("English Suite", "Englische Suite", _name)
	set _name to my replaceWords("French Suite", "Französische Suite", _name)
	set _name to my replaceWords("Overture in the French Style", "Ouvertüre nach Französischer Art", _name)

	set _name to my replaceWords("Italian Konzert", "Concerto nach Italienischen Gusto", _name)
	set _name to my replaceText("for keyboard ", "", _name)
	set _name to my replaceText(", for keyboard", "", _name)
	set _name to my replaceText(", for solo keyboard", "", _name)

	set _name to my replaceText("Six Little Preludes", "Sechs kleine Präludien", _name)
	set _name to my replaceWords("Three Little Fugues", "Drei Kleine Fugen", _name)

	if _name does not contain "Messiah" then
		set _name to my replaceText(" and ", " und ", _name)
	end if

	set _name to my replaceWords("Little Prelude", "Präludium", _name)
	set _name to my replaceWords("on a theme by", "über ein Thema von", _name)

	set _name to my replaceWords("after", "nach", _name)
	set _name to my replaceWords("fur", "für", _name)
	set _name to my replaceText(", BWV 933-938 (BC L64-69)", "", _name)
	set _name to my replaceWords("The Art of the Fugue", "Die Kunst der Fuge", _name)
	set _name to my replaceWords("Chromatic Fantasia und Fugue", "Chromatische Fantasie und Fuge", _name)
	set _name to my replaceText(" from Klavierbuchlein für Wilhelm Bach", "", _name)
	set _name to my replaceText(" (Clavier-Übung II/1), BWV 971", ", BWV 971:", _name)
	-- set _name to my replaceText(" (Klavierbüchlein)", "", _name)

	set _name to my stripIncidental("BC", _name)

	if 826 is _bwv then
		-- 826 Sinfonia. Grave Adagio - Andante
	else if 846 ≤ _bwv and _bwv ≤ 893 then
		if not my startsWith(_name, "Bach: ") and not my startsWith(_name, "Bach (CPE") then
			set _name to "Bach: " & _name
		end if
		if _bwv ≤ 869 and not my startsWith(_name, "Bach: Das Wohltemperierte Klavier, Buch 1") then
			set _name to my replaceText("Bach: ", "Bach: Das Wohltemperierte Klavier, Buch 1 - ", _name)
		end if
		if _bwv ≥ 870 and not my startsWith(_name, "Bach: Das Wohltemperierte Klavier, Buch 2") then
			set _name to my replaceText("Bach: ", "Bach: Das Wohltemperierte Klavier, Buch 2 - ", _name)
		end if
		set _name to my replaceText("Bach: Das Wohltemperierte Klavier, Buch 2: ", "Bach: Das Wohltemperierte Klavier, Buch 2 - ", _name)
		set _name to my replaceWords("Prelude in es-Moll and Fugue in dis-Moll no. 8, BWV 853", "Präludium in es-Moll und Fuge in dis-Moll No. 8, BWV 853", _name)
		set _name to my replaceWords("Prelude", "I. Praeludium", _name)
		set _name to my replaceWords("Fugue", "II. Fuga", _name)
		set _name to my replaceText(" no. ", " No. ", _name)
	else if 898 is _bwv then
		set _name to "Präludium und Fuge, B-Dur (über BACH), BWV 898"
	else if 900 is _bwv then
		set _name to my replaceText("BWV 900 ", "BWV 900: ", _name)
	else if 996 is _bwv then
		set _name to my replaceWords("(G minor Version)", "(g-Moll)", _name)
		-- else if 999 ≤ _bwv and _bwv ≤ 1000 then
		-- set _name to my replaceWords("Prelude", "Präludium", _name)
		-- set _name to my replaceWords("Fugue", "Fuge", _name)
	else if 1011 is _bwv then
		set _name to my replaceWords("Suite Per La Viola Da Gamba in d-Moll, BWV 1011", "Suite für Viola da Gamba No. 5 in c-Moll, BWV 1011", _name)
	else if 1014 ≤ _bwv and _bwv ≤ 1019 then
		set _name to my replaceWords("Violin Sonata", "Sonate für Violine und Cembalo", _name)
	else if 1025 is _bwv then
		set _name to my replaceWords("Trio in A-Dur, BWV 1025", "Triosonate in A-Dur, BWV 1025 (Suite für Violine und Cembalo obligato nach Silvius Leopold Weiss)", _name)
	else if 1027 ≤ _bwv and _bwv ≤ 1029 then
		set _name to my replaceWords("Viola Da Gamba Sonata", "Sonate für Viola da Gamba und Cembalo", _name)
		set _name to my replaceWords("Sonata", "Sonate für Viola da Gamba und Cembalo", _name)
		set _name to my replaceWords("Sonate für Viola da Gamba und Cembalo in g-Moll, BWV 1029", "Sonate für Viola da Gamba und Cembalo No. 3 in g-Moll, BWV 1029", _name)
	else if 1030 ≤ _bwv and _bwv ≤ 1035 then
		set _name to my replaceWords("Flute Sonata", "Sonate für Flöte und Cembalo", _name)
	else if 1043 is _bwv then
		set _name to my replaceWords("Konzert in d-Moll für zwei Violinen", "Doppelkonzert für zwei Violinen d-Moll", _name)
	else if 1052 ≤ _bwv and _bwv ≤ 1058 then
		set _name to my replaceWords("Keyboard Konzert", "Klavierkonzert", _name)
		set _name to my replaceWords("Konzert", "Klavierkonzert", _name)
	else if 1080 is _bwv then
		set _name to my replaceText(" [Excerpts]", "", _name)
	end if

	-- Händel
	set _hwv to my getHWVFrom(_name)
	set _name to my replaceText("Concerto A Due Cori", "Concerto a due cori", _name)
	set _name to my replaceText("Konzert A Due Cori", "Concerto a due cori", _name)
	set _name to my replaceText("Music For The Royal Fireworks", "Royal Fireworks Music", _name)

	if 426 ≤ _hwv and _hwv ≤ 433 then
		set _name to my replaceText("Harpsichord Suite", "Suites de pièce pour le clavecin Vol. 1", _name)
	else if 434 ≤ _hwv and _hwv ≤ 442 then
		set _name to my replaceText("Harpsichord Suite", "Suites de pièce pour le clavecin Vol. 2", _name)
	end if

	if _hwv = 435 then
		set _name to my replaceText("Chaconne & Variations", "Chaconne mit 21 Variationen", _name)
	end if

	set _name to my replaceText("Israel In Egypt", "Israel in Egypt", _name)
	set _name to my replaceText("Organ Konzert", "Orgelkonzert", _name)
	set _name to my replaceText("The Cuckoo & The Nightingale", "The Cuckoo and the Nightingale", _name)

	set _name to my replaceWords("Prelude", "Präludium", _name)
	set _name to my replaceWords("Fugue", "Fuge", _name)

	-- Haydn
	set _name to my replaceText("The Bear", "L'Ours", _name)
	set _name to my replaceText("The Hen", "La Poule", _name)
	set _name to my replaceText("The Queen", "La Reine", _name)
	set _name to my replaceText("The Philosopher", "Der Philosoph", _name)

	set _name to my replaceWords("Violin Sonata", "Violinsonate", _name) -- don't move up
	set _name to my replaceText("Klaviersonate for Violin und Piano", "Violinsonate", _name) -- don't move up
	set _name to my replaceText("Violinsonate no.", "Violinsonate No.", _name) -- don't move up

	set _name to my replaceText("#", "No. ", _name)
	set _name to my normalizeTempo(_name)

	return _name
end normalizeClassicalTrackGermanName

--
--
--
to normalizeClassicalTrackFrenchName(_name)
	set _name to my replaceWords("Major", "major", _name)
	set _name to my replaceWords("Minor", "minor", _name)
	set _name to my replaceWords("Flat", "flat", _name)
	set _name to my replaceWords("Sharp", "sharp", _name)
	set _name to my replaceWords("C-Sharp", "C sharp", _name)
	set _name to my replaceWords("D-Sharp", "D sharp", _name)
	set _name to my replaceWords("F-Sharp", "F sharp", _name)
	set _name to my replaceWords("G-Sharp", "G sharp", _name)
	set _name to my replaceWords("A-Flat", "A flat", _name)
	set _name to my replaceWords("B-Flat", "B flat", _name)
	set _name to my replaceWords("E-Flat", "E flat", _name)

	-- https://fr.wikipedia.org/wiki/Note_de_musique	set _name to my replaceWords("In C major", "in do majeur", _name)
	set _name to my replaceWords("In C minor", "en do mineur", _name)
	set _name to my replaceWords("In C flat major", "en do♭ mineur", _name)
	set _name to my replaceWords("In C flat minor", "en do♭ mineur", _name)
	set _name to my replaceWords("In C flat", "en do♭ mineur", _name)
	set _name to my replaceWords("In C sharp major", "en do♯ majeur", _name)
	set _name to my replaceWords("In C sharp minor", "en do♯ mineur", _name)
	set _name to my replaceWords("In C sharp", "en do♯ majeur", _name)
	set _name to my replaceText(" In C ", " en do majeur ", _name)
	set _name to my replaceText(" In C,", " en do majeur,", _name)
	set _name to my replaceText(" In C", " en do majeur ", _name)

	set _name to my replaceWords("In D major", "en ré majeur", _name)
	set _name to my replaceWords("In D minor", "en ré mineur", _name)
	set _name to my replaceWords("In D flat major", "en ré♭ majeur", _name)
	set _name to my replaceWords("In D flat minor", "en ré♭ mineur", _name)
	set _name to my replaceWords("In D flat", "en ré♭ majeur", _name)
	set _name to my replaceWords("In D sharp major", "en ré♯ majeur", _name)
	set _name to my replaceWords("In D sharp minor", "en ré♯ mineur", _name)
	set _name to my replaceWords("In D sharp", "en ré♯ majeur", _name)
	set _name to my replaceText(" In D ", " en ré majeur ", _name)
	set _name to my replaceText(" In D,", " en ré majeur,", _name)

	set _name to my replaceWords("In E major", "en mi majeur", _name)
	set _name to my replaceWords("In E minor", "en mi mineur", _name)
	set _name to my replaceWords("In E flat major", "en mi♭ majeur", _name)
	set _name to my replaceWords("In E flat minor", "en mi♭ mineur", _name)
	set _name to my replaceWords("In E flat", "en mi♭ majeur", _name)
	set _name to my replaceText(" In E ", " en mi majeur ", _name)
	set _name to my replaceText(" In E,", " en mi majeur,", _name)

	set _name to my replaceWords("In F major", "en fa majeur", _name)
	set _name to my replaceWords("In F minor", "en fa mineur", _name)
	set _name to my replaceWords("In F sharp major", "en fa♯ majeur", _name)
	set _name to my replaceWords("In F sharp minor", "en fa♯ mineur", _name)
	set _name to my replaceWords("In F sharp", "en fa♯ majeur", _name)
	set _name to my replaceText(" In F ", " en fa majeur ", _name)
	set _name to my replaceText(" In F,", " en fa majeur,", _name)
	set _name to my replaceText(" In F", " en fa majeur ", _name)

	set _name to my replaceWords("In G major", "en sol majeur", _name)
	set _name to my replaceWords("In G minor", "en sol mineur", _name)
	set _name to my replaceWords("In G flat major", "en sol♭ majeur", _name)
	set _name to my replaceWords("In G flat minor", "en sol♭ mineur", _name)
	set _name to my replaceWords("In G flat", "en sol♭ majeur", _name)
	set _name to my replaceWords("In G sharp major", "en sol♯ majeur", _name)
	set _name to my replaceWords("In G sharp minor", "en sol♯ mineur", _name)
	set _name to my replaceWords("In G sharp", "en sol♯ majeur", _name)
	set _name to my replaceText(" In G ", " en sol majeur ", _name)
	set _name to my replaceText(" In G,", " en sol majeur,", _name)

	set _name to my replaceWords("In A major", "en la majeur", _name)
	set _name to my replaceWords("In A minor", "en la mineur", _name)
	set _name to my replaceWords("In A flat major", "en la♭ majeur", _name)
	set _name to my replaceWords("In A flat minor", "en la♭ mineur", _name)
	set _name to my replaceWords("In A flat", "en la♭ majeur", _name)
	set _name to my replaceWords("In A sharp major", "en la♯ majeur", _name)
	set _name to my replaceWords("In A sharp minor", "en la♯ mineur", _name)
	set _name to my replaceWords("In A sharp", "en la♯ majeur", _name)
	set _name to my replaceText(" In A ", " en la majeur ", _name)
	set _name to my replaceText(" In A,", " en la majeur,", _name)

	set _name to my replaceWords("In B major", "en si majeur", _name)
	set _name to my replaceWords("In B minor", "en si mineur", _name)
	set _name to my replaceWords("In B flat major", "en si♭ majeur", _name)
	set _name to my replaceWords("In B flat minor", "en si♭ mineur", _name)
	set _name to my replaceWords("In B flat", "en si♭ majeur", _name)
	set _name to my replaceText(" In B ", " en si majeur ", _name)
	set _name to my replaceText(" In B,", " en si majeur,", _name)

	set _name to my replaceText("majeur major", "majeur", _name)
	set _name to my replaceText("in F minor/A flat", "en fa mineur/la♭ majeur", _name)

	set _name to my replaceText(",Op.", ", Op.", _name)
	set _name to my replaceText("Op.1", "Op. 1", _name)
	set _name to my replaceText("Op.2", "Op. 2", _name)
	set _name to my replaceText("Op.3", "Op. 3", _name)
	set _name to my replaceText("Op.4", "Op. 4", _name)
	set _name to my replaceText("Op.5", "Op. 5", _name)
	set _name to my replaceText("Op.6", "Op. 6", _name)
	set _name to my replaceText("Op.7", "Op. 7", _name)
	set _name to my replaceText("Op.8", "Op. 8", _name)
	set _name to my replaceText("Op.9", "Op. 9", _name)
	set _name to my replaceText("No.1", "No. 1", _name)
	set _name to my replaceText("No.2", "No. 2", _name)
	set _name to my replaceText("No.3", "No. 3", _name)
	set _name to my replaceText("No.4", "No. 4", _name)
	set _name to my replaceText("No.5", "No. 5", _name)
	set _name to my replaceText("No.6", "No. 6", _name)
	set _name to my replaceText("No.7", "No. 7", _name)
	set _name to my replaceText("No.8", "No. 8", _name)
	set _name to my replaceText("No.9", "No. 9", _name)

	set _name to my replaceText("Etude", "Étude", _name)
	set _name to my replaceText("Études, Op.", "Étude Op. ", _name)
	set _name to my replaceText("Préludes, Op.", "Prélude Op. ", _name)
	set _name to my replaceText("Prelude", "Prélude", _name)
	set _name to my replaceWords("Waltz", "Valse", _name)
	set _name to my replaceWords("Piano Concerto", "Concerto pour piano", _name)
	set _name to my replaceWords("Piano Sonata", "Sonate pour piano", _name)
	set _name to my replaceText("#", "n° ", _name)
	set _name to my replaceText(" No. ", " n° ", _name)
	set _name to my replaceText(" No.", " n° ", _name)
	set _name to my replaceWords("Andante Spianato et Grand Polonaise Brillante", "Andante spianato et Grande Polonaise brillante", _name)
	set _name to my replaceText("Funeral March", "Marche funèbre", _name)

	set _name to my replaceText(". en", " en", _name)
	set _name to my replaceText("- n°", " n°", _name)
	set _name to my replaceText("  ", " ", _name)

	-- Dmitrij Dmitrievič Šostakovič
	set _name to my replaceText("Shostakovich: ", "Šostakovič: ", _name)

	-- Aleksandr Nikolaevič Skrjabin
	set _name to my replaceText("Scriabin: ", "Skrjabin: ", _name)
	set _name to my replaceText("Three Pieces", "Trois Morceaux", _name)
	set _name to my replaceText("Four Pieces", "Quatre Pièces", _name)

	set _name to my normalizeTempo(_name)

	return _name
end normalizeClassicalTrackFrenchName



--
--
--
to stripIncidental(_start, _name)
	set _leading to "(" & _start

	if _name contains _leading then
		set _bc to offset of _leading in _name
		set _s1 to text 1 thru (_bc - 2) of _name
		set _name to (text (_bc + 3) thru (length of _name) of _name & "  ")
		set _par to offset of ")" in _name
		set _s2 to text (_par + 2) thru (length of _name) of _name
		set _name to _s1 & _s2

		repeat while (text -1 thru -1 of _name) is " "
			set _name to text 1 thru -2 of _name
		end repeat
	end if

	return _name
end stripIncidental

--
--
--
to getBWVFrom(_name)
	return my getCatalogFrom(_name, "BWV")
end getBWVFrom

--
--
--
to getHWVFrom(_name)
	return my getCatalogFrom(_name, "HWV")
end getHWVFrom

--
--
--
to getCatalogFrom(_name, acronym)
	if _name does not contain acronym then
		return 0
	end if

	set _name to _name & " "
	set _i to offset of acronym in _name
	set _remainer to text (_i + 4) thru (length of _name) of _name
	set _j to offset of " " in my replaceText(":", " ", _remainer)
	set _bwv to text 1 thru (_j - 1) of _remainer

	try
		return _bwv as number
	on error line number num
		return 0
	end try
end getCatalogFrom

--
--
--
to normalizeTempo(_name)
	set _name to my replaceWords("Staccato Ma Non Troppo Allegro", "Staccato ma non troppo allegro", _name)
	set _name to my replaceWords("Largo E Staccato", "Largo e staccato", _name)
	set _name to my replaceWords("Organo Ad Libitum", "Organo ad libitum", _name)
	set _name to my replaceWords("Grave, Organo Ad Libitum", "Grave, Organo ad libitum", _name)
	set _name to my replaceWords("Lentement (Organo Ad Libitum )", "Lentement (Organo ad libitum)", _name)
	set _name to my replaceWords("A Tempo Ordinario", "A tempo ordinario", _name)
	set _name to my replaceWords("Allegro Ma Non Presto", "Allegro ma non presto", _name)
	set _name to my replaceWords("A Tempo Ordinario E Staccato, Adagio", "A tempo ordinario e staccato, Adagio", _name)
	set _name to my replaceWords("Aria Con Variazioni", "Aria con variazioni", _name)
	set _name to my replaceWords("Adagio E Staccato", "Adagio e staccato", _name)
	set _name to my replaceWords("A Tempo Giusto", "A tempo giusto", _name)
	set _name to my replaceWords("Andante Larghetto", "Andante larghetto", _name)
	set _name to my replaceWords("Largo E Piano", "Largo e piano", _name)
	set _name to my replaceWords("Andante larghetto E Staccato", "Andante larghetto e staccato", _name)

	set _name to my replaceWords("Allegro Con Fuoco", "Allegro con fuoco", _name)
	set _name to my replaceWords("Presto Con Fuoco", "Presto con fuoco", _name)
	set _name to my replaceWords("Prestissimo Volando", "Prestissimo volando", _name)
	set _name to my replaceWords("Allegro Maestoso", "Allegro maestoso", _name)

	set _name to my replaceWords("Andante Un Poco Sustenito", "Andante un poco sostenuto", _name)
	set _name to my replaceWords("Andante Un Poco Sostenuto", "Andante un poco sostenuto", _name)
	set _name to my replaceWords("Allegro Maestro", "Allegro maestro", _name)
	set _name to my replaceWords("Finale Rondo", "Finale rondò", _name)

	set _name to my replaceText("Tempo di Minuetto", "Tempo di minuetto", _name)
	set _name to my replaceText("Tempo di Gavotta", "Tempo di gavotta", _name)

	set _name to my replaceWords("Vivace Alla Marcia", "Vivace alla marcia", _name)
	set _name to my replaceWords("Allegretto Ma Non Troppo", "Allegretto ma non troppo", _name)
	set _name to my replaceWords("Adagio Sostenuto", "Adagio sostenuto", _name)
	set _name to my replaceWords("Presto Agitato", "Presto agitato", _name)
	set _name to my replaceWords("Allegro molto Moto", "Allegro molto moto", _name)
	set _name to my replaceWords("Allegro Ma Mon Troppo", "Allegro ma non troppo", _name)
	set _name to my replaceWords("Rondo: Allegro", "Rondo: allegro", _name)
	set _name to my replaceWords("Adagio Un Poco Mosso", "Adagio un poco mosso", _name)
	set _name to my replaceWords("Andante Cantabile Con Moto", "Andante cantabile con moto", _name)
	set _name to my replaceWords("Menuetto. Allegro molto E Vivace", "Menuetto. Allegro molto e vivace", _name)
	set _name to my replaceWords("Adagio. Allegro molto E Vivace", "Adagio. Allegro molto e vivace", _name)
	set _name to my replaceWords("Adagio, Ma Non Troppo", "Adagio, ma non troppo", _name)
	set _name to my replaceWords("Scherzo & Trio: Allegro", "Scherzo & trio: allegro", _name)

	set _name to my replaceWords("Poco Sostenuto, Vivace", "Poco sostenuto, vivace", _name)
	set _name to my replaceWords("Marcia Funebre, Adagio Assai", "Marcia funebre, adagio assai", _name)
	set _name to my replaceWords("Scherzo: Allegro vivace", "Scherzo: allegro vivace", _name)
	set _name to my replaceWords("Finale: Allegro molto", "Finale: allegro molto", _name)
	set _name to my replaceWords("Adagio, Allegro Vivace", "", _name)
	set _name to my replaceWords("Andante Cantabile", "Andante cantabile", _name)
	set _name to my replaceWords("La Malinconia: Adagio - Allegretto Quasi Allegro", "La malinconia: adagio - allegretto quasi allegro", _name)
	set _name to my replaceWords("Andante Un Poco Maestoso", "Andante un poco maestoso", _name)
	set _name to my replaceWords("Scherzo: Molto Vivace", "Scherzo: molto vivace", _name)
	set _name to my replaceWords("Allegro Animato E Grazioso", "Allegro animato e grazioso", _name)
	set _name to my replaceWords("Sostenuto Assai", "Sostenuto assai", _name)
	set _name to my replaceWords("Scherzo: Allegro vivace", "Scherzo: allegro vivace", _name)
	set _name to my replaceWords("Adagio Espressivo", "Adagio espressivo", _name)
	set _name to my replaceWords("Allegro molto Vivace", "Allegro molto vivace", _name)
	set _name to my replaceWords("Allegro molto Moderato", "Allegro molto moderato", _name)
	set _name to my replaceWords("Menuetto: Allegretto", "Menuetto: allegretto", _name)

	set _name to my replaceWords("Allegro Affettuoso", "Allegro affettuoso", _name)
	set _name to my replaceWords("Intermezzo: Andantiono Grazioso", "Intermezzo: andantino grazioso", _name)
	set _name to my replaceWords("Scherzo Ed Intermezzo", "Scherzo ed intermezzo", _name)
	set _name to my replaceWords("Andante Un Poco Mosso", "Andante un poco mosso", _name)
	set _name to my replaceWords("Adagio E Dolce", "Adagio e dolce", _name)
	set _name to my replaceWords("Adagio E Spiccato", "Adagio e spiccato", _name)
	set _name to my replaceWords("Adagio Molto allegro", "Adagio molto allegro", _name)
	set _name to my replaceWords("Adagio Molto", "Adagio molto", _name)
	set _name to my replaceText("Adagio Non Troppo", "Adagio non troppo", _name)
	set _name to my replaceWords("Allegretto Grazioso (Quasi Andantino)", "Allegretto grazioso (quasi andantino)", _name)
	set _name to my replaceWords("Allegretto Moderato", "Allegretto moderato", _name)
	set _name to my replaceText("Allegretto Molto Moderato E Comodo; Un Poco Più Animato", "Allegretto molto moderato e comodo - Un poco più animato", _name)
	set _name to my replaceWords("Allegro Assai", "Allegro assai", _name)
	set _name to my replaceWords("Allegro Appassionato", "Allegro appassionato", _name)
	set _name to my replaceWords("Allegro Comodo", "Allegro comodo", _name)
	set _name to my replaceWords("Allegro Con Brio", "Allegro con brio", _name)
	set _name to my replaceWords("Allegro Con Spirito", "Allegro con spirito", _name)
	set _name to my replaceWords("Allegro Di Molto", "Allegro di molto", _name)
	set _name to my replaceWords("Allegro Energico E Passionato", "Allegro energico e passionato", _name)
	set _name to my replaceWords("Allegro Giocoso", "Allegro giocoso", _name)
	set _name to my replaceText("Allegro Giocoso, Ma Non Troppo Vivace", "Allegro giocoso, ma non troppo vivace", _name)
	set _name to my replaceWords("Allegro Grazioso", "Allegro grazioso", _name)
	set _name to my replaceWords("Allegro Ma Non Tanto", "Allegro ma non tanto", _name)
	set _name to my replaceWords("Allegro Ma Non Troppo", "Allegro ma non troppo", _name)
	set _name to my replaceWords("Allegro Moderato", "Allegro moderato", _name)
	set _name to my replaceWords("Allegro Molto", "Allegro molto", _name)
	set _name to my replaceWords("Allegro Non Assai", "Allegro non assai", _name)
	set _name to my replaceWords("Allegro Non Molto", "Allegro non molto", _name)
	set _name to my replaceText("Allegro Non Troppo", "Allegro non troppo", _name)
	set _name to my replaceWords("Allegro Spiritoso", "Allegro spiritoso", _name)
	set _name to my replaceWords("Allegro Vivace", "Allegro vivace", _name)
	set _name to my replaceWords("Andante Con Moto", "Andante con moto", _name)
	set _name to my replaceWords("Andante E Spiccato", "Andante e spiccato", _name)
	set _name to my replaceWords("Andante Grazioso", "Andante grazioso", _name)
	set _name to my replaceWords("Andante Largo", "Andante largo", _name)
	set _name to my replaceWords("Andante Moderato", "Andante moderato", _name)
	set _name to my replaceWords("Andante Molto", "Andante molto", _name)
	set _name to my replaceWords("Andante Sostenuto", "Andante sostenuto", _name)
	set _name to my replaceWords("Andante, Allegro", "Andante, allegro", _name)
	set _name to my replaceWords("Andantino Cantabile", "Andantino cantabile", _name)
	set _name to my replaceWords("Andantino Sostenute E Cantabile", "Andantino sostenuto e cantabile", _name)
	set _name to my replaceWords("Finale: Allegro Assai", "Finale: allegro assai", _name)
	set _name to my replaceWords("Grave E Sempre Piano", "Grave e sempre piano", _name)
	set _name to my replaceWords("Larghetto E Spiritoso", "Larghetto e spiritoso", _name)
	set _name to my replaceWords("Larghetto E Spirituoso", "Larghetto e spirituoso", _name)
	set _name to my replaceWords("Largo E Spiccato", "Largo e spiccato", _name)
	set _name to my replaceWords("L'Istesso Tempo, Ma Grazioso", "L'istesso tempo, ma grazioso", _name)
	set _name to my replaceWords("Marcia: Maestoso", "Marcia: maestoso", _name)
	set _name to my replaceWords("Menuet & Trio", "Menuetto e trio", _name)
	set _name to my replaceWords("Menuetto (Allegro Molto)", "Menuetto (allegro molto)", _name)
	set _name to my replaceWords("Molto Allegro", "Molto allegro", _name)
	set _name to my replaceWords("Non Mosso E Molto Marcato", "Non mosso e molto marcato", _name)
	set _name to my replaceWords("Pastorale Ad Libitum", "Pastorale ad libitum", _name)
	set _name to my replaceWords("Piu Andante", "Più andante", _name)
	set _name to my replaceWords("Più Andante", "Più andante", _name)
	set _name to my replaceText("Quasi Menuetto, Moderato; Allegro Vivace", "Quasi menuetto, moderato - Allegro vivace", _name)
	set _name to my replaceText("Romanze: Poco Adagio", "Romanze: poco adagio", _name)
	set _name to my replaceWords("Rondeau: Allegretto Grazioso", "Rondeau: allegretto grazioso", _name)
	set _name to my replaceWords("Rondeau: Allegro", "Rondeau: allegro", _name)
	set _name to my replaceWords("Rondeau: Andante Grazioso", "Rondeau: andante grazioso", _name)
	set _name to my replaceWords("Rondò Allegro", "Rondò allegro", _name)
	set _name to my replaceWords("Rondò: Allegretto", "Rondò: allegretto", _name)
	set _name to my replaceWords("Rondò: Andantino Con Moto", "Rondò: andantino con moto", _name)
	set _name to my replaceWords("Rondò: Andantino", "Rondò: andantino", _name)
	set _name to my replaceWords("Tema Con Variazioni", "Tema con variazioni", _name)
	set _name to my replaceWords("Tempo Di Menuetto", "Tempo di menuetto", _name)
	set _name to my replaceWords("Un Poco Allegro", "Un poco allegro", _name)
	set _name to my replaceWords("Un Poco Allegretto E Grazioso", "Un poco allegretto e grazioso", _name)
	set _name to my replaceWords("Un Poco Sostenuto", "Un poco sostenuto", _name)

	set _name to my replaceWords("Allegro, Andante, allegro Da Capo", "Allegro, Andante, Allegro da capo", _name)
end normalizeTempo

--
--
--
to normalizeWolfgangAmadeusMozart(_name, _track)
	set _name to my replaceText(" K. ", " K ", _name)

	set _name to my normalizeClassicalTrackGermanName(_name, _track, "")

	--	tell application "iTunes"
	--		set _album to name of album of _track
	--	end tell

	--	TODO: if _album is "Mozart: Sonatas for Piano and Violin" then
	set _name to my replaceWords("Violinsonate", "Sonate für Klavier und Violine", _name)
	--	end if

	if _name contains "364" then
		set _name to my replaceWords("Sinfonia Concertante", "Sinfonia concertante für Violine und Viola", _name)
	end if

	if _name contains "297b" then
		set _name to my replaceWords("Sinfonia Concertante", "Sinfonia concertante für Flöte, Oboe, Horn und Fagott", _name)
	end if

	-- ""
	set _name to my replaceWords("Le Nozze Di Figaro", "Le nozze di Figaro", _name)

	set _name to my replaceWords("Perchè", "Perché", _name)
	set _name to my replaceWords("perchè", "perché", _name)

	return _name
end normalizeWolfgangAmadeusMozart

--
--
--
to normalizeAntonioVivaldi(_name)
	set _name to my replaceWords("Chamber Concerto", "Concerto da camera", _name)
	set _name to my replaceWords("Concerti Per Viola D'amore", "Concerto per viola d'amore in", _name)
	set _name to my replaceWords("Il Proteo O Sia Il Mondo Al Rovescio", "Il Proteo o sia Il mondo al rovescio", _name)
	set _name to my replaceWords("Tromba Marina", "tromba marina", _name)
	set _name to my replaceWords("L’Estro Armonico", "L’estro armonico", _name)
	set _name to my replaceWords("La Stravaganza", "La stravaganza", _name)
	set _name to my replaceWords("La Tempesta Di Mare", "La tempesta di mare", _name)
	set _name to my replaceWords("La Notte", "La notte", _name)
	set _name to my replaceWords("Il Sonno", "Il sonno", _name)
	set _name to my replaceWords("Il Sospetto", "Il sospetto", _name)
	set _name to my replaceWords("La Caccia", "La caccia", _name)
	set _name to my replaceWords("Concerto Funebre", "Concerto funebre", _name)
	set _name to my replaceWords("Il Piacere", "Il piacere", _name)
	set _name to my replaceWords("Il Riposo - Per Il Natale", "Il Riposo - Per il Natale", _name)
	set _name to my replaceWords("Per Eco In Lontano", "Per eco in lontano", _name)

	set _name to my replaceWords("The Four Seasons", "Le quattro stagioni", _name)
	set _name to my replaceWords("Spring", "Primavera", _name)
	set _name to my replaceWords("Summer", "Estate", _name)
	set _name to my replaceWords("Autumn", "Autunno", _name)
	set _name to my replaceWords("Winter", "Inverno", _name)
	set _name to my replaceWords("(Primavera)", "- Primavera", _name)
	set _name to my replaceWords("(Estate)", "- Estate", _name)
	set _name to my replaceWords("(Autunno)", "- Autunno", _name)
	set _name to my replaceWords("(Inverno)", "- Inverno", _name)
	set _name to my replaceWords("Danza Pastorale", "Danza pastorale", _name)

	set _name to my replaceWords("& Orchestra", "e orchestra", _name)
	set _name to my replaceText(" & ", " e ", _name)

	set _name to my normalizeClassicalTrackItalianName(_name)

	set _name to my replaceText(",\"", "\",", _name)
	set _name to my replaceText(" (\"", ", \"", _name)
	set _name to my replaceText("\")", "\"", _name)

	return _name
end normalizeAntonioVivaldi

--
--
--
to normalizeArcangeloCorelli(_name)
	set _name to my replaceWords("Christmas Concerto", "Fatto per la notte di Natale", _name)
	set _name to my replaceWords("'fatto per la notte di Natale'", "\"Fatto per la notte di Natale\"", _name)
	set _name to my normalizeClassicalTrackItalianName(_name)

	return _name
end normalizeArcangeloCorelli

--
--
--
to normalizeHildegardVonBingen(_name)
	set _name to my replaceWords("Hildegard:", "Bingen:", _name)
	set _name to my replaceWords("Hildegard:", "Bingen:", _name)
	set _name to my replaceWords("Hildegard von Bingen:", "Bingen:", _name)
	set _name to my replaceWords("sainte Ursule", "Sainte Ursule", _name)

	if _name does not contain ":" then
		set _name to "Bingen: " & _name
	end if

	return _name

end normalizeHildegardVonBingen

--
--
--
to normalizeClaudioMonteverdi(_name)
	set _name to my replaceWords("Madrigals, Book 1 (Il primo libro de madrigali 1587): ", "Madrigali, Primo libro - ", _name)
	set _name to my replaceWords("Madrigals, Book 2 (Il secondo libro de madrigali 1590): ", "Madrigali, Secondo libro - ", _name)
	set _name to my replaceWords("Madrigals, Book 3: ", "Madrigali, Terzo libro - ", _name)
	set _name to my replaceWords("Madrigals, Book 7 (Il settimo libro de madrigali 1619): ", "Madrigali, Settimo libro - ", _name)
	set _name to my replaceWords("The Sixth Book of Madrigals: ", "Madrigali, Sesto libro - ", _name)
	set _name to my replaceWords("Madrigals, Book 7 (Il settimo libro de madrigali 1619): ", "Madrigali, Settimo libro - ", _name)
	set _name to my replaceWords("Madrigals, Book 8 (Madrigali guerrieri et amorosi...libro ottavo), Madrigali guerrieri,", "Madrigali, Ottavo libro (guerrieri) - ", _name)
	set _name to my replaceWords("Madrigals, Book 8 (Madrigali guerrieri et amorosi...libro ottavo), Madrigali guerrieri: ", "Madrigali, Ottavo libro (guerrieri) - ", _name)
	set _name to my replaceWords("Ottavo Libro (1698) \"Madrigali guerrieri et amorosi\"", "Madrigali, Ottavo libro (amorosi)", _name)
	set _name to my replaceWords("Madrigals, Book 8 (L'ottavo libro de madrigali, 1638: Balli), ", "Madrigali, Ottavo libro (balli) - ", _name)
	set _name to my replaceWords("wds.", "testo", _name)

	return _name
end normalizeClaudioMonteverdi

--
--
--
to normalizeVincenzoBellini(_name)
	set _name to my replaceWords("Norma,", "Norma:", _name)
	set _name to my replaceWords("ACT", "Atto", _name)
	set _name to my replaceWords("Scene", "scena", _name)

	return _name

end normalizeVincenzoBellini


--
--
--
to normalizeComposer(_track)
	tell application "Music"
		set _composer to composer of _track
		set _sortComposer to sort composer of _track

		if _sortComposer is "" then
			if _composer is not "" and (((offset of ";" in _composer) > 0) or ((offset of "/" in _composer) > 0)) then -- multiple composers not supported so far
				return _composer
			end if

			if _composer is "Anonymous" then
				set _sortComposer to "@Anonymous" -- put at the top of the list
			else
				set _i to offset of ", " in _composer

				if _i > 0 then
					set _sortComposer to _composer
					set _lastName to text 1 thru (_i - 1) of _sortComposer
					set _firstName to text (_i + 2) thru (length of _sortComposer) of _sortComposer
					set _composer to _firstName & " " & _lastName
				end if

			end if

			my trace("    composer: " & _sortComposer & " -> " & _composer)

			if not DRY_RUN then
				set composer of _track to _composer
				set sort composer of _track to _sortComposer
			end if
		end if
	end tell

	return _sortComposer
end normalizeComposer

--
--
--
to setExtendedAttribute(_track, _name, _value)
	tell application "Music"
		set _comment to comment of _track
		set _newComment to ""
		set _attributes to my split(_comment, ATTR_SEPARATOR)
		set _separator to ""
		set _replaced to false
		set _newAttribute to _name & ATTR_COLON & _value

		--my trace("ATTRIBUTES: " & _attributes)

		repeat with _attribute in _attributes
			--my trace("ATTRIBUTE: " & _attribute)
			if my startsWith(_attribute, _name) then
				set _attribute to _newAttribute
				set _replaced to true
			end if
			set _newComment to _newComment & _separator & _attribute
			set _separator to ATTR_SEPARATOR
		end repeat

		if not _replaced then
			set _newComment to _newComment & _separator & _newAttribute
		end if

		--my trace("NEW COMMENT ---> " & _newComment)

		considering case, diacriticals, hyphens, punctuation and white space
			if _newComment is not _comment and not DRY_RUN then
				set comment of _track to _newComment
			end if
		end considering
	end tell
end setExtendedAttribute

--
--
--
to getExtendedAttribute(_track, _name, _defaultValue)
	tell application "Music"
		set _comment to comment of _track
		set _attributes to my split(_comment, ATTR_SEPARATOR)

		--my trace("ATTRIBUTES: " & _attributes)

		repeat with _attribute in _attributes
			--my trace("ATTRIBUTE: " & _attribute)
			if my startsWith(_attribute, _name) then
				return text (1 + (length of (_name & ATTR_COLON))) thru (length of _attribute) of _attribute
			end if
		end repeat
	end tell

	return _defaultValue
end getExtendedAttribute

--
--
--
to startsWith(_string, _prefix)
	considering case, diacriticals, hyphens, punctuation and white space
		if (length of _string) < (length of _prefix) then
			return false
		end if

		return (text 1 thru (length of _prefix) of _string) is _prefix
	end considering
end startsWith

--
--
--
to normalizeClassicalTrackNames()
	tell application "Music"
		set _dump to ""
		set _renamedCount to 0

		set _librarySource to the source named "Library"
		set _oldfi to fixed indexing
		set fixed indexing to true

		set _tracks to tracks in _librarySource

		(*
		set _albums to audio CD playlists in _librarySource

		repeat with _album in _albums
			my trace("ALBUM: " & name of _album)
		end repeat
		*)

		my trace("Processing " & (count of _tracks) & " tracks...")


		repeat with _track in _tracks
			set _shelf to my getExtendedAttribute(_track, ATTR_SHELF, "")

			if _shelf is "Classical" then
				set _originalName to my getExtendedAttribute(_track, ATTR_ORIGINAL_NAME, name of _track)
				my trace(_originalName)
				set _show to show of _track
				set _kind to kind of _track
				set _processed to false

				if _kind is "Apple Lossless audio file" then
					set _composer to my normalizeComposer(_track)

					if _composer is "Vivaldi, Antonio" then
						set _newName to my normalizeAntonioVivaldi(_originalName)
						set _processed to true
					end if

					if _composer is "Corelli, Arcangelo" then
						set _newName to my normalizeArcangeloCorelli(_originalName)
						set _processed to true
					end if

					if {"Albinoni, Tomaso", "Marcello, Alessandro", "Cherubini, Luigi", "Viotti, Giovanni Battista", "Paganini, Niccolò", "Boccherini, Luigi", "Frescobaldi, Girolamo", "Gabrielli, Domenico", "Jacchini, Giuseppe Maria", "degli Antonii, Giovanni Battista"} contains _composer then
						set _newName to my normalizeClassicalTrackItalianName(_originalName)
						set _processed to true
					end if

					if {"Chopin, Frederyk", "Skrjabin, Aleksandr Nikolaevič", "Šostakovič, Dmitrij Dmitrievič"} contains _composer then
						set _newName to my normalizeClassicalTrackFrenchName(_originalName)
						set _processed to true
					end if

					if _composer is "Bingen, Hildegard Von" then
						set _newName to my normalizeHildegardVonBingen(_originalName)
						set _processed to true
					end if

					if _composer is "Bellini, Vincenzo" then
						set _newName to my normalizeVincenzoBellini(_originalName)
						set _processed to true
					end if

					if _composer is "Mozart, Wolfgang Amadeus" then
						set _newName to my normalizeWolfgangAmadeusMozart(_originalName, _track)
						set _processed to true
					end if

					if {"Beethoven, Ludwig van", "Brahms, Johannes", "Mahler, Gustav", "Schumann, Robert", "Schubert, Franz", "Bach, Johann Sebastian", ¬
						"Bach, Carl Philipp Emanuel", "Spohr, Louis", "Händel, Georg Friedrich", "Haydn, Franz Joseph"} contains _composer then
						set _newName to my normalizeClassicalTrackGermanName(_originalName, _track, _composer)
						set _processed to true
					end if

					if _composer is "Monteverdi, Claudio" then
						set _newName to my normalizeClaudioMonteverdi(_originalName)
						set _processed to true
					end if


					if _processed then
						if _composer contains "," then
							set _composerPrefix to text 1 thru ((offset of "," in _composer) - 1) of _composer & ": "
						else
							set _composerPrefix to _composer & ": "
						end if

						set _newName to my replaceText("Handel", "Händel", _newName)

						if not my startsWith(_newName, _composerPrefix) and not my startsWith(_newName, "Marcello A.") then
							set _newName to _composerPrefix & _newName
						end if

						my trace("    -> " & _newName)
						--set show of _track to _show
						my setExtendedAttribute(_track, ATTR_ORIGINAL_NAME, _originalName)

						considering case, diacriticals, hyphens, punctuation and white space
							if _newName is not name of _track then
								if not DRY_RUN then
									my trace("       ******** applying change")
									set name of _track to _newName
								end if
								set _renamedCount to _renamedCount + 1
							end if
						end considering

						set _dump to _dump & _originalName & CRLF & _newName & CRLF & CRLF
					end if
				end if
			end if
		end repeat
		set fixed indexing to _oldfi

		my trace("Tracks renamed: " & _renamedCount)

		my writeToFile(_dump, "/Volumes/Users/fritz/Personal/Music/Normalizer/test/actual normalized track names.txt")

	end tell

end normalizeClassicalTrackNames

--
-- Writes a text to a file.
--
-- _string:			the text
-- _path:				the path of the file
--
to writeToFile(_string, _path)
	if DEBUG then
		my trace("Writing to " & _path)
	end if

	try
		tell current application
			set _file to (open for access _path with write permission)
			set eof of the _file to 0
			write _string to the _file starting at eof as «class utf8»
			close access _file
		end tell
	on error _error
		log "Error: " & _error
		try
			close access _file
		end try
	end try
end writeToFile

--
--
--
to replaceWords(find, replace, subject)
	return my replaceTextOrWords(find, replace, subject, true)
end replaceWords

--
--
--
to replaceText(find, replace, subject)
	return my replaceTextOrWords(find, replace, subject, false)
end replaceText

--
--
--
to replaceTextOrWords(_find, _replace, _string, _words)
	set ALPHABET to "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" -- accented?

	set _i to offset of _find in _string -- starts from 1

	if (_i = 0) then
		return _string
	end if

	set _prevIndex to _i - 1
	set _afterIndex to _i + (length of _find)
	set _replaceNow to _replace

	if (_prevIndex > 0) then
		set _before to text 1 thru _prevIndex of _string
	else
		set _before to ""
	end if

	if (_afterIndex ≤ length of _string) then
		set _after to text _afterIndex thru (length of _string) of _string
	else
		set _after to ""
	end if

	if (_words and _prevIndex > 0 and _afterIndex ≤ length of _string) then
		set _charBefore to text _prevIndex of _string
		set _charAfter to text _afterIndex of _string

		ignoring diacriticals
			if (ALPHABET contains _charBefore or ALPHABET contains _charAfter) then
				set _replaceNow to text (_prevIndex + 1) thru (_afterIndex - 1) of _string
			end if
		end ignoring
	end if

	return _before & _replaceNow & my replaceTextOrWords(_find, _replace, _after, _words)
end replaceTextOrWords

--
--
--
to split(_string, _delimiter)
	set _oldDelimiters to AppleScript's text item delimiters
	set AppleScript's text item delimiters to _delimiter
	set _array to every text item of _string
	set AppleScript's text item delimiters to _oldDelimiters
	return _array
end split

--on replaceText2(find, replace, subject)
--	set prevTIDs to text item delimiters of AppleScript
--	set text item delimiters of AppleScript to find
--	set subject to text items of subject
--
--	set text item delimiters of AppleScript to replace
--	set subject to subject as text
--	set text item delimiters of AppleScript to prevTIDs
--
--	return subject
--end replaceText2


--
-- Asserts a mandatory condition and eventually fails.
--
-- _condition:			the condition to check
-- _reason: 			the reason to fail
--
to assert(_condition, _reason)
	if not _condition then
		my fail(_reason)
	end if
end assert

--
-- Fails the script because of the given reason.
--
-- _reason: 			the reason to fail
--
to fail(_reason)
	trace("Failing: " & _reason)
	error number -128
end fail

--
-- Logs a message for debugging purposes.
--
-- _message: 			the message to log
--
to trace(message)
	if DEBUG then
		log ">>>> " & message
	end if
end trace


